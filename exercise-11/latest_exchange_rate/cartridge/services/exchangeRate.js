'use strict';
var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

var getLatestExchangeRate = function getLatestExchangeRate(serviceName) {
    var myHttpService = LocalServiceRegistry.createService(serviceName, {
        createRequest: function (service) {
            service.setRequestMethod("GET");
        },
        parseResponse: function (service, response) {
            return response.text;
        }
    });

    var result = myHttpService.call();

    return result;
}

module.exports.getLatestExchangeRate = getLatestExchangeRate;