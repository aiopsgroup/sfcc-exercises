'use strict';

var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var XMLStreamReader = require('dw/io/XMLStreamReader');
var XMLStreamConstants = require('dw/io/XMLStreamConstants');
var FileWriter = require('dw/io/FileWriter');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var PriceBookMgr = require('dw/catalog/PriceBookMgr');
var exchangeRateService = require('*/cartridge/services/exchangeRate');

/**
 * @function exchangePrice
 * @description Function that convert prices from one price book to another based on conversion rate by service api call
 *
 * @param {Object} parameters Represents the parameters defined in the steptypes.json file
 */
var exchangePrice = function exchangePrice(parameters) {
        var exchangeRate = null;
        var exportedPriceBook = PriceBookMgr.getPriceBook(parameters.BasePriceBookID);
        var priceBookToConvert = PriceBookMgr.getPriceBook(parameters.ConvertedPriceBookID);

        if(!exportedPriceBook || !priceBookToConvert) {
                var missedPriceBook = !exportedPriceBook ? parameters.BasePriceBookID : parameters.ConvertedPriceBookID;
                throw new Error('Price book with name ' + missedPriceBook + ' does not exist');
        }

        //call service api
        var result = exchangeRateService.getLatestExchangeRate(parameters.ServiceName);

        if (result.status == 'OK') {
                try {
                        var conversionObject = JSON.parse(result.object);
                } catch (e) {
                        throw new Error('Api result parsed to json failed.');
                }

                if (conversionObject.base != exportedPriceBook.getCurrencyCode()) {
                        throw new Error('Base currency code from conversion service and currency code from exported price book doesn\'t match');       
                }

                if (!conversionObject.rates[priceBookToConvert.getCurrencyCode()]) {
                        throw new Error('There is no conversion rate currency matched with the currency from ' + priceBookToConvert.getID() + ' price book');       
                }

                exchangeRate = conversionObject.rates[priceBookToConvert.getCurrencyCode()];
        } else {
                throw new Error('Service api call failed');
        }

        if (!exchangeRate) {
                throw new Error('Exchange rate not found');
        }

        //File for reading prices
        var exportedFilePath = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + parameters.ExportFolder + File.SEPARATOR + parameters.BasePriceBookID + '.xml';
        var priceBookFile = new File(exportedFilePath);
        var fileReader = new FileReader(priceBookFile);
        var xmlStreamReader = new XMLStreamReader(fileReader);

        //check if directory exist (if not create it)
        var directoryPath = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + parameters.ImportFolder;
        var theDirectory = new File(directoryPath);
        if (!theDirectory.exists()) {
                theDirectory.mkdirs();
        }

        //Creation of the new file used to fill with the prices after conversion
        var newFilePath = directoryPath + File.SEPARATOR + parameters.ConvertedPriceBookID + '.xml';
        var newFile = new File(newFilePath);
        newFile.createNewFile();

        var fileWriter = new FileWriter(newFile, "UTF-8");
        var newPriceBookFile = new XMLStreamWriter(fileWriter);

        newPriceBookFile.writeStartDocument();
        newPriceBookFile.writeStartElement("pricebooks");
        newPriceBookFile.writeAttribute("xmlns", "http://www.demandware.com/xml/impex/pricebook/2006-10-31");
        newPriceBookFile.writeStartElement("pricebook");

                newPriceBookFile.writeStartElement("header");
                newPriceBookFile.writeAttribute("pricebook-id", priceBookToConvert.getID());
                        newPriceBookFile.writeStartElement("currency");
                        newPriceBookFile.writeCharacters(priceBookToConvert.getCurrencyCode());
                        newPriceBookFile.writeEndElement();
                        newPriceBookFile.writeStartElement("display-name");
                        newPriceBookFile.writeCharacters(priceBookToConvert.getDisplayName());
                        newPriceBookFile.writeEndElement();
                        //TODO: onlineFrom and onlineTo needed to be considered too
                        newPriceBookFile.writeStartElement("online-flag");
                        newPriceBookFile.writeCharacters(priceBookToConvert.isOnline());
                        newPriceBookFile.writeEndElement();
                newPriceBookFile.writeEndElement();

        newPriceBookFile.writeStartElement("price-tables");

        while (xmlStreamReader.hasNext()) {
                xmlStreamReader.next();
                var streamEventType = xmlStreamReader.getEventType();

                if (streamEventType == XMLStreamConstants.START_ELEMENT) {
                        var localElementName = xmlStreamReader.getLocalName();
                        if (localElementName == "price-table") {
                                //price table element
                                var priceTable = xmlStreamReader.readXMLObject();
                                var productIdAttribute = priceTable.attribute('product-id').toString();
                                newPriceBookFile.writeStartElement(localElementName);
                                newPriceBookFile.writeAttribute("product-id", productIdAttribute);

                                //amount elements inside parent (price-table)
                                var amountElements = priceTable.elements();
                                var amountCount = amountElements.length();
                                for (let index = 0; index < amountCount; index++) {
                                        var amount = amountElements[index];
                                        var amountAttribute = amount.attribute('quantity').toString();
                                        var amountElementName = amount.localName();
                                        
                                        newPriceBookFile.writeStartElement(amountElementName);
                                        if(amountAttribute) {
                                                newPriceBookFile.writeAttribute("quantity", amountAttribute);
                                        }

                                        var exchangedPrice = amount * exchangeRate;
                                        newPriceBookFile.writeCharacters(exchangedPrice);

                                        newPriceBookFile.writeEndElement();
                                }

                                newPriceBookFile.writeEndElement();
                        }
                }
        }

        //price-tables/pricebook/pricebooks end elements
        newPriceBookFile.writeEndElement();
        newPriceBookFile.writeEndElement();
        newPriceBookFile.writeEndElement();
        newPriceBookFile.writeEndDocument();

        xmlStreamReader.close();
        fileReader.close();

        newPriceBookFile.close();
        fileWriter.close();
}

module.exports.exchangePrice = exchangePrice;