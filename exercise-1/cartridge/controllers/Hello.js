'use strict';

var server = require('server');

/**
 * This is a JSDoc. It is intended to provide information on the function below it,
 * improving readability and code quality.
 * It is more than just a comment block. It also has its own usable tags, which start with '@'.
 * You will often see functions describe the parameters they are expecting,
 * as well as what the return value of the function is.
 * Check out more at: https://jsdoc.app/about-getting-started.html
 */
server.get('World', function (req, res, next) {
    res.render('exercise/helloWorld');
    next();
});

module.exports = server.exports();
