'use strict';

var server = require('server');
var URLUtils = require('dw/web/URLUtils');

server.get(
    'Show',
    server.middleware.https,
    function (req, res, next) {
        var actionUrl = URLUtils.url('Newsletter-Handler');
        var newsletterForm = server.forms.getForm('newsletter');
        newsletterForm.clear();

        res.render('/newsletter/newsletter', {
            actionUrl: actionUrl,
            newsletterForm: newsletterForm
        });

        next();
    }
);

server.post(
    'Handler',
    server.middleware.https,
    function (req, res, next) {
        var newsletterForm = server.forms.getForm('newsletter');
        var continueUrl = URLUtils.url('Newsletter-Show');

        if (newsletterForm.valid) {
            // Show the success page
            res.json({
                success: true,
                redirectUrl: dw.web.URLUtils.url('Newsletter-Success').toString()
            });
        } else {
            // Handle server-side validation errors here: this is just an example
            res.setStatusCode(500);
            res.json({
                error: true,
                redirectUrl: continueUrl
            });
        }

        next();
    }
);

server.get(
    'Success',
    server.middleware.https,
    function (req, res, next) {
        res.render('/newsletter/success', {
            continueUrl: URLUtils.url('Newsletter-Show'),
            newsletterForm: server.forms.getForm('newsletter')
        });

        next();
    }
);

module.exports = server.exports();
