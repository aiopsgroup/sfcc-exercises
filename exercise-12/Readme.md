## How to include link_stripe cartridge in project, besides code deployment

### Code Deployment

Build your scrips and make sure they are uploaded, along with the new cartridges, added. Double check in Business Manager.

1. Visit Administration > Sites > Manage Sites.

2. Select your current site name.

3. Select tab Settings.

4. Under the current cartridges list, you can see all available cartridges listed. You need to have Stripe’s cartridges in the list as well.

### Edit Cartridge's Path

It is important that you add the new cartridges in Site’s cartridge path. Add app_stripe_sfra:int_stripe_sfra:int_stripe_core to the cartridge path, in front of your current site’s cartridges, so the new integrations may use their code for base and only enrich current site’s possibilities. It does not have the core code functionality inside.

### Metadata

You can also notice that there is metadata included. It should be uploaded in Business Manager to the corresponding locations.

#### Meta Metadata

1. In folder metadata you have the stripe_site_template folder. You need to archive it and import on Administration > Site Development > Site Import & Export page.

2. Under Import > Upload Archive tab, please Choose File… and upload your archive.

3. Then, select it via the checkbox and click Import.

4. Upon Success message, you can verify whether the Metadata is in place. However, there is something small, which is placed inside Metadata folder, but does not upload automatically and should be additionally set. The Payment Processors…

#### Payment Processors

1. In folder metadata\stripe_site_template\sites\siteIDHere, you can find payment-processors.xml. It contains the Processors, needed for the Payment Methods to run. Observing the XML structure, you can find that there are two of them, which should be added in BM.

2. You can do it manually via: Merchant Tools > Ordering > Payment Processors.

3. There click New. All you need is to add one by one the following Processors, copying the exact names as in the XML file, i.e. STRIPE_CREDIT and STRIPE_APM.

4. Verify the result: They must be visible on the Payment Processors page.