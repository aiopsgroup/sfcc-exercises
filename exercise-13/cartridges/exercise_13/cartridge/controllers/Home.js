'use strict';

var server = require('server');
server.extend(module.superModule);

/**
 * Creates an string from Site Preference values
 * @param {Object} countryGreetings - Site Preference set of values
 * @param {String} countryCode - Current Site's Country Code
 * @returns {String} a string, containing the local greeting
 */
 function getCountryGreeting(countryGreetings, countryCode) {

    var result = '';
    for(var i=0; i<countryGreetings.length; i++) {
        for(var key in countryGreetings[i]) {
            if(countryGreetings[i].indexOf(countryCode)!=-1) {
                result = countryGreetings[i].slice(3);
            }
        }
    }
    return result;
}


/**
 *
 */
server.append('Show', function (req, res, next) {
    var Site = require('dw/system/Site');
    var viewData = res.getViewData();

    var countryGreetings = Site.current.getCustomPreferenceValue("localGreetings");
    var countryCode = req.geolocation.countryCode;

    var countryGreeting = getCountryGreeting(countryGreetings, countryCode);

    viewData.countryGreeting = countryGreeting;
    res.setViewData(viewData);

    next();
});

module.exports = server.exports();
