'use strict';

var server = require('server');

server.get('Start', function (req, res, next) {
    var today = new Date(Date.now());

    res.render('practice/practicePage', {
        today: today
    });
    next();
});

server.get('Continue', function (req, res, next) {
    var usersInfo = [{
        name: 'Mary',
        likes: ['Sports', 'Ice Cream', 'Cars'],
        dislikes: ['Winter']
    },
    {
        name: 'Ben',
        likes: ['Poker', 'TV'],
        dislikes: ['Loud Music', 'Crowds']
    },
    {
        name: 'Susan',
        likes: ['Dogs'],
        dislikes: ['Cats', 'Birds', 'Insects']
    }
    ];
    res.render('practice/practiceContinue', {
        usersInfo: usersInfo
    });
    next();
});

module.exports = server.exports();
