'use strict';

var server = require('server');
server.extend(module.superModule);

/**
 *
 */
server.append('Show', function (req, res, next) {
    var viewData = res.getViewData();
    var isPromotionActive = true;
    var promotionMessage = 'Promotional prices! Buy now!';
    viewData.isPromotionActive = isPromotionActive;
    viewData.promotionMessage = promotionMessage;


    res.setViewData(viewData);
    next();
});

module.exports = server.exports();
