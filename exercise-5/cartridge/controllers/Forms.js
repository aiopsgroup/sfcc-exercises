'use strict';

var server = require('server');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

server.use('Basic', function (req, res, next) {
  var message = req.form.message || '';
  res.render('basicForm', {
    message: message
  });
  next();
});

server.use('Calc', csrfProtection.generateToken, function (req, res, next) {
  var calcForm = server.forms.getForm('calc');
  var formProperties = calcForm.calculator;

  var CSRFProtection = require('dw/web/CSRFProtection');

  if (req.httpMethod === 'GET') {
    calcForm.clear();
  } else {
    var csrfCheck = CSRFProtection.validateRequest();
    if (!csrfCheck) {
      throw new Error('The CSRF token check was not passed.');
    }
  }

  if (!formProperties.valid) {
    res.render('calculator', {
      formProperties: formProperties,
      error: true
    });
    return next();
  }

  var num1 = +formProperties.number1.value;
  var num2 = +formProperties.number2.value;
  var operator = formProperties.operator.value;
  var result;

  switch (operator) {
    case '+':
      result = num1 + num2;
      break;
    case '-':
      result = num1 - num2;
      break;
    case '*':
      result = num1 * num2;
      break;
    case '/':
      if (num2 === 0) {
        result = 0;
        break;
      }
      result = num1 / num2;
      break;
    default:
      result = 0;
      break;
  }

  res.render('calculator', {
    formProperties: formProperties,
    result: result
  });
  next();
});

module.exports = server.exports();
