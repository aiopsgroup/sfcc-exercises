'use strict';

var server = require('server');
var ProductMgr = require('dw/catalog/ProductMgr');

server.extend(module.superModule);

server.get('Product', function (req, res, next) {
  var product = ProductMgr.getProduct('al-rihla');

  res.render('practice/productPage', {
    product: product
  });
  next();
});

module.exports = server.exports();
