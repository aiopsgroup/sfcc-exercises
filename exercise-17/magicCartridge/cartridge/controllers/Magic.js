var server = require('server');
var service = require('magicCartridge/cartridge/services/dadjokeservice');

server.get('Show', function (req, res, next) {
    var properties = {};

    var svcResult = service.dadJokeAPIService.call();
    if (svcResult.status === 'OK') {
        properties.joke = svcResult.object.joke;
    }

    res.render('magic', properties);
    next();
});

module.exports = server.exports();
