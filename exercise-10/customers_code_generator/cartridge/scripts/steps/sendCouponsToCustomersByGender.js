'use strict';

var CustomerMgr = require('dw/customer/CustomerMgr');
var CouponMgr = require('dw/campaign/CouponMgr');
var HookMgr = require('dw/system/HookMgr');
var Transaction = require('dw/system/Transaction');


/**
 * @function sendCoupons
 * @description Function that send coupons to all customers by gender param
 *
 * @param {Object} parameters Represents the parameters defined in the steptypes.json file
 */
var sendCoupons = function sendCoupons(parameters) {
        var gender = parameters.Gender.toString();
        if(gender != '1' && gender != '2') {
                throw new Error('Gender cannot be different than 1(Male) or 2(Female)');
        }

        var genderQuery = "gender=" + gender;
        var customers = CustomerMgr.searchProfiles(genderQuery, null);

        var Coupon = CouponMgr.getCoupon(parameters.CouponId);
        if (!Coupon.isEnabled()) {
                throw new Error('Coupon is not enabled');
        }

        var couponType = Coupon.getType();
        var couponPromotion = Coupon.getPromotions().toArray();
        couponPromotion = couponPromotion.pop();
        var promoMsg = couponPromotion.calloutMsg.markup;

        var emailTemplate = 'coupon/couponEmail.isml';
        while (customers.hasNext()) {
                var customer = customers.next();
                var customerEmail = customer.email;

                Transaction.wrap(function () {
                        //this code needs to be in transaction in order to work
                        if (couponType == 'SINGLE_CODE') {
                                var couponCode = Coupon.getID();
                        } else {
                                // diffrent couponCode for every user
                                var couponCode = Coupon.getNextCouponCode();
                        }

                        var templateData = {
                                firstName: customer.firstName,
                                promoMsg: promoMsg,
                                couponCode: couponCode
                        };

                        //send user email with coupon code
                        HookMgr.callHook('newsletter.email', 'send', customerEmail, emailTemplate, templateData);

                });
        }
}


module.exports.sendCoupons = sendCoupons;