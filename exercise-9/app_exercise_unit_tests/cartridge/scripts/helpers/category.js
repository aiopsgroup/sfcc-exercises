'use strict';

var CatalogMgr = require('dw/catalog/CatalogMgr');

/**
 * Check Product category is custom category
 * @param {dw.catalog.Category} apiCategory category for given product
 * @returns {boolean} Product category is test category
 */
function isCustomCategory(apiCategory) {
    var myBrandCategory = CatalogMgr.getCategory('MyBrand');

    var parentCategory = apiCategory;
    while (parentCategory) {
        if (parentCategory.getID() === myBrandCategory.getID()) {
            return true;
        }
        parentCategory = parentCategory.parent;
    }

    return false;
}

module.exports = {
    isCustomCategory: isCustomCategory
};
