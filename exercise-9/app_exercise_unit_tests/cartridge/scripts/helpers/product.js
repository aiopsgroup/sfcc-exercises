'use strict';

/**
 * Build product name
 * @param {Object} product product
 * @returns {string} a new name
 */
function buildCustomProductName(product) {
    var name = '';
    if (product.name) {
        name = product.productType + ' - ' + product.name;
    }

    return name;
}

module.exports = {
    buildCustomProductName: buildCustomProductName
};
