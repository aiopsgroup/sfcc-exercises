'use strict';

var ProductMgr = require('dw/catalog/ProductMgr');
var categoryHelper = require('*/cartridge/scripts/helpers/category');
var Resource = require('dw/web/Resource');

/**
 * Add product description title
 * @param {string} productId product ID
 * @returns {Object} product
 */
function addProductDescriptionTitle(productId) {
    if (!productId) {
        return null;
    }

    var product = ProductMgr.getProduct(productId);
    if (!product) {
        return null;
    }

    var primaryCategory = product.primaryCategory;
    if (categoryHelper.isCustomCategory(primaryCategory)) {
        product.descriptionTitle = Resource.msg('product.custom.category.title', 'product', null);
    } else {
        product.descriptionTitle = Resource.msg('product.title.category', 'product', null);
    }

    return product;
}

module.exports = {
    addProductDescriptionTitle: addProductDescriptionTitle
};
