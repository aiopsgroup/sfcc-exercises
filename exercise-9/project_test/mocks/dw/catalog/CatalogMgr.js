'use strict';

/**
 * Get Category
 * @param {string} categoryId category ID
 * @returns {dw.catalog.Category} category object
 */
function getCategory(categoryId) {
    return {
        getID: function () {
            return categoryId;
        }
    };
}

module.exports = {
    getCategory: getCategory
};
