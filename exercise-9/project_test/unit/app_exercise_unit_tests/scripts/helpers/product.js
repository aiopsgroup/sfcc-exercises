'use strict';

var assert = require('chai').assert;

describe('Product', function () {
    var productHelpers = require('../../../../../cartridges/app_exercise_unit_tests/cartridge/scripts/helpers/product');

    var productData = {
        name: ''
    };

    describe('buildCustomProductName() function', function () {
        it('should return an empty name when product does not have name', function () {
            var expectedResults = '';
            var results = productHelpers.buildCustomProductName(productData);

            assert.equal(results, expectedResults);
        });

        it('should combine product type and name when product has name', function () {
            productData = {
                name: 'Test',
                productType: 'Variation'
            };

            var expectedResults = 'Variation - Test';
            var results = productHelpers.buildCustomProductName(productData);

            assert.equal(results, expectedResults);
        });
    });
});
