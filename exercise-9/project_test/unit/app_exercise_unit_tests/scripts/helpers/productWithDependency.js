'use strict';

var assert = require('chai').assert;
var sinon = require('sinon');
var proxyquire = require('proxyquire').noPreserveCache().noCallThru();

var isCustomCategoryStub = sinon.stub();
var msgStub = sinon.stub();
var getProductStub = sinon.stub();

const DUMMY_PRODUCT_ID = 1;

describe('Product with dependency', function () {
    var product2Helpers = proxyquire('../../../../../cartridges/app_exercise_unit_tests/cartridge/scripts/helpers/productWithDependency', {
        'dw/catalog/ProductMgr': {
            getProduct: getProductStub
        },
        '*/cartridge/scripts/helpers/category': {
            isCustomCategory: isCustomCategoryStub
        },
        'dw/web/Resource': {
            msg: msgStub
        }
    });

    afterEach(function () {
        sinon.restore();
    });

    describe('addProductDescriptionTitle() function', function () {
        it('should return null when product id is null', function () {
            var results = product2Helpers.addProductDescriptionTitle(null);

            assert.equal(results, null);
        });

        it('should return null when there is no product', function () {
            getProductStub.returns(null);
            var results = product2Helpers.addProductDescriptionTitle(DUMMY_PRODUCT_ID);

            assert.equal(results, null);
        });

        it('should return custom description title when product is assigned to custom category', function () {
            getProductStub.returns({});
            isCustomCategoryStub.returns(true);
            msgStub.returns('My special brand');
            var results = product2Helpers.addProductDescriptionTitle(DUMMY_PRODUCT_ID);

            var expectedResults = 'My special brand';

            assert.equal(results.descriptionTitle, expectedResults);
            sinon.assert.calledWith(msgStub, 'product.custom.category.title', 'product', null);
        });

        it('should return description title when product is not assigned to custom category', function () {
            getProductStub.returns({});
            isCustomCategoryStub.returns(false);
            msgStub.returns('Simple brand');

            var results = product2Helpers.addProductDescriptionTitle(DUMMY_PRODUCT_ID);

            var expectedResults = 'Simple brand';

            assert.equal(results.descriptionTitle, expectedResults);
            sinon.assert.calledWith(msgStub, 'product.title.category', 'product', null);
        });
    });
});
