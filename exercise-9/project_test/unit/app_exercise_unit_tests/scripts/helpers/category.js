'use strict';

var assert = require('chai').assert;
var proxyquire = require('proxyquire').noPreserveCache().noCallThru();

var CatalogMgr = require('../../../../mocks/dw/catalog/CatalogMgr');

describe('Category', function () {
    var categoryHelpers = proxyquire('../../../../../cartridges/app_exercise_unit_tests/cartridge/scripts/helpers/category', {
        'dw/catalog/CatalogMgr': CatalogMgr
    });

    describe('isCustomCategory() function', function () {
        it('should return true when category is a custom one', function () {
            var categoryData = {
                getID: function () {
                    return 'Test';
                },
                parent: {
                    getID: function () {
                        return 'MyBrand';
                    }
                }
            };

            var results = categoryHelpers.isCustomCategory(categoryData);

            assert.isTrue(results);
        });

        it('should return false when category is not custom one', function () {
            var categoryData = {
                getID: function () {
                    return 'Test1';
                }
            };

            var results = categoryHelpers.isCustomCategory(categoryData);

            assert.isFalse(results);
        });
    });
});
